package test.java;


public abstract class Driver {

	public static BridgeProject getBridge() {
		BridgeProxy bridge = new BridgeProxy();

		bridge.setRealBridge(new RealBridge()); // add real bridge here
		return bridge;
	}	
}