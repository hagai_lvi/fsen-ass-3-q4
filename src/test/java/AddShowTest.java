package test.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.*;
import test.java.auxiliary.ShowInfo;


public class AddShowTest extends ProjectTest {
	
	private ShowInfo goodShow1;
	private ShowInfo goodShow2;
	
	protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	
	@Before
	public void setUp() {
		super.setUp();
		setUpGoodShows();
	}
	
	private void setUpGoodShows() {
		this.goodShow1=new ShowInfo();
		this.goodShow1.city = (String)this.halls[0][HALL_CITY];
		this.goodShow1.hall = (String)this.halls[0][HALL_NAME];
		this.goodShow1.description = "show description here";
		this.goodShow1.name = "an example show";
		this.goodShow1.ticketCost = 120;
		try {
			this.goodShow1.lastOrderTimeDate = dateFormat.parse("20.1.2014 23:59:59").getTime();
			this.goodShow1.showTimeDate = dateFormat.parse("20.1.2015 20:00:00").getTime();
		} catch (ParseException e) {
			this.goodShow1.lastOrderTimeDate = System.currentTimeMillis()+ 1000*60*60*24*30;
			this.goodShow1.showTimeDate = this.goodShow1.lastOrderTimeDate+ 1000*60*60*24*3;
		}
		
		this.goodShow2=new ShowInfo();
		this.goodShow2.city = (String)this.halls[1][HALL_CITY];
		this.goodShow2.hall = (String)this.halls[1][HALL_NAME];
		this.goodShow2.description = "another description here";
		this.goodShow2.name = "another example";
		this.goodShow2.ticketCost = 30;
		try {
			this.goodShow2.lastOrderTimeDate = dateFormat.parse("25.12.2014 23:59:59").getTime();
			this.goodShow2.showTimeDate = dateFormat.parse("30.01.2015 20:00:00").getTime();
		} catch (ParseException e) {
			this.goodShow2.lastOrderTimeDate = System.currentTimeMillis()+ 1000*60*60*24*30;
			this.goodShow2.showTimeDate = this.goodShow2.lastOrderTimeDate+ 1000*60*60*24*3;
		}		
	}

	@After
	public void tearDown() {
		this.goodShow1=null;
		this.goodShow2=null;
	}
	
	@Test
	public void testAddShow() {
		int showId1 = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], this.goodShow1);
		assertTrue(showId1 > 0);
		int showId2 = this.addShow(this.users[1][USER_USER], this.users[1][USER_PASS], this.goodShow2);
		assertTrue(showId2 > 0);
		assertTrue("Different shows must have different ids !", showId1 != showId2);
	}
	

	@Test
	public void testAddShowWrongUserLoc() {
		int showId;
		showId = this.addShow(this.users[1][USER_USER], this.users[1][USER_PASS], this.goodShow1);
		assertEquals(this.users[1][USER_CITY]+" admin add show to "+this.goodShow1.city,0,showId);
		showId = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], this.goodShow2);
		assertEquals(this.users[0][USER_CITY]+" admin add show to "+this.goodShow2.city,0,showId);
	}
	
	@Test
	public void testAddShowWrongUserOrPass() {
		int showId;
		showId = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS]+"xxx", this.goodShow1);
		assertEquals(0,showId);
		showId = this.addShow(this.users[1][USER_USER], this.users[0][USER_PASS], this.goodShow2);
		assertEquals(0,showId);
		showId = this.addShow("noExists", this.users[1][USER_PASS], this.goodShow2);
		assertEquals(0,showId);
		showId = this.addShow(null, this.users[0][USER_PASS], this.goodShow1);
		assertEquals(0,showId);
		showId = this.addShow(this.users[1][USER_USER], null, this.goodShow2);
		assertEquals(0,showId);
	}
	
	@Test
	public void testAddShowWrongDates() {
		switchShowDates(goodShow1);
		int showId1 = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], this.goodShow1);
		assertEquals(0,showId1);
		switchShowDates(goodShow2);
		int showId2 = this.addShow(this.users[1][USER_USER], this.users[1][USER_PASS], this.goodShow2);
		assertEquals(0,showId2);
	}
	
	private void switchShowDates(ShowInfo show) {
		long tmp = show.lastOrderTimeDate;
		show.lastOrderTimeDate = show.showTimeDate;
		show.showTimeDate = tmp;
	}
	
	@Test
	public void testAddShowMissingLoc() {
		goodShow1.city=null;
		int showId1 = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], this.goodShow1);
		assertEquals(0,showId1);
		goodShow2.hall=null;
		int showId2 = this.addShow(this.users[1][USER_USER], this.users[1][USER_PASS], this.goodShow2);
		assertEquals(0,showId2);
	}	
}