package test.java;

import org.junit.Before;
import org.junit.Test;
import test.java.auxiliary.OrderInfo;
import test.java.auxiliary.ShowInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class OrderTest extends ProjectTest {

	protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	
	private int futureShowId, pastShowId;
	
	private OrderInfo goodOrder1, goodOrder2;
	
	@Before
	public void setUp() {
		super.setUp();
		setUpShows();
		setUpOrders();
	}
	
	private void setUpShows() {
		ShowInfo show=new ShowInfo();
		show.city = (String)this.halls[0][HALL_CITY];
		show.hall = (String)this.halls[0][HALL_NAME];
		show.description = "show description here";
		show.name = "an example show";
		show.ticketCost = 120;
		try {
			show.lastOrderTimeDate = dateFormat.parse("30.05.2015 23:59:59").getTime();
			show.showTimeDate = dateFormat.parse("20.06.2015 20:00:00").getTime();
		} catch (ParseException e) {
			show.lastOrderTimeDate = System.currentTimeMillis()+ 1000*60*60*24*30;
			show.showTimeDate = show.lastOrderTimeDate+ 1000*60*60*24*3;
		}
		
		this.futureShowId = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], show);
		assertTrue(this.futureShowId > 0);
		this.reserveMemberChairs(futureShowId,1,30);

        show=new ShowInfo();
        show.city = (String)this.halls[0][HALL_CITY];
        show.hall = (String)this.halls[0][HALL_NAME];
        show.description = "past show description";
        show.name = "past show name";
        show.ticketCost = 150;
		try {
			show.lastOrderTimeDate = dateFormat.parse("01.12.2014 23:59:59").getTime();
			show.showTimeDate = dateFormat.parse("03.05.2015 20:00:00").getTime();
		} catch (ParseException e) {		
			show.lastOrderTimeDate = System.currentTimeMillis() - 1000*60*60*24*30;
			show.showTimeDate = show.lastOrderTimeDate + 1000*60*60*24*3;
		}
		this.pastShowId = this.addShow(this.users[0][USER_USER], this.users[0][USER_PASS], show);
        assertTrue(this.pastShowId > 0);
		this.reserveMemberChairs(pastShowId,1,30);
	}
	
	private void setUpOrders() {
		goodOrder1 = new OrderInfo(); 
		goodOrder1.name = "Ariela";
		goodOrder1.phone = "03-6940177";
		goodOrder1.chairsIds = new int[] { 50,51,52,53 };
		goodOrder1.memberId = "";
		goodOrder1.showId = this.futureShowId;

		goodOrder2 = new OrderInfo(); 
		goodOrder2.name = "Ariel";
		goodOrder2.phone = "03-6940320";
		goodOrder2.chairsIds = new int[] { 5,6 };
		goodOrder2.memberId = "11122322";
		goodOrder2.showId = this.futureShowId;		
	}

	

	public void testPlaceOrder() {
		int reservationId1 = this.placeOrder(goodOrder1);
        assertTrue(reservationId1 > 0);
		int reservationId2 = this.placeOrder(goodOrder2);


        assertTrue(reservationId2 > 0);
		assertTrue("Different orders should have different ids !",reservationId1 != reservationId2);
	}
	
	@Test
	public void testPlaceOrderNotMember() {
		goodOrder2.memberId = null;
		int reservationId = this.placeOrder(goodOrder2);
		assertTrue("Only Pais members can order reserved chairs !",reservationId == 0);
	}
	
	@Test
	public void testPlacePastOrder() {
		goodOrder1.showId = this.pastShowId;
		goodOrder2.showId = this.pastShowId;
		int reservationId;
		reservationId = this.placeOrder(goodOrder1);
		assertTrue("member can not order tickets after last order date !",reservationId == 0);
		reservationId = this.placeOrder(goodOrder2);
		assertTrue("member can not order tickets after last order date !",reservationId == 0);
	}

	@Test
	public void testPlaceUnknownOrder() {
		goodOrder1.showId = -243;
		do {
		 goodOrder2.showId = (int)(Math.random()*10000)+100;
		} while(goodOrder2.showId== this.futureShowId || goodOrder2.showId== this.pastShowId);
		int reservationId;
		reservationId = this.placeOrder(goodOrder1);
		assertTrue("member can not order tickets to unknown show !",reservationId == 0);
		reservationId = this.placeOrder(goodOrder2);
		assertTrue("member can not order tickets to unknown show !",reservationId == 0);
	}
	
	@Test
	public void testPlaceOrderMissingInfo1() {
		goodOrder1.name = null;
		goodOrder2.phone = null;
		int reservationId;
		reservationId = this.placeOrder(goodOrder1);
		assertTrue("member can not order tickets without name !",reservationId == 0);
		reservationId = this.placeOrder(goodOrder2);
		assertTrue("member can not order tickets without phone number !",reservationId == 0);
	}	
	@Test
	public void testPlaceOrderMissingInfo2() {
		goodOrder1.chairsIds = null;
		goodOrder2.chairsIds = new int[0];
		int reservationId;
		reservationId = this.placeOrder(goodOrder1);
		assertTrue("member can not order tickets without chairs !",reservationId == 0);
		reservationId = this.placeOrder(goodOrder2);
		assertTrue("member can not order tickets without chairs !",reservationId == 0);
	}	
}