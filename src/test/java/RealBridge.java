package test.java;

import main.java.PaisFacade;
import test.java.auxiliary.OrderInfo;
import test.java.auxiliary.ShowInfo;

/**
 * Created by hagai_lvi on 12/23/14.
 */
public class RealBridge implements BridgeProject {
    private PaisFacade facade;
    public RealBridge(){
        facade = new PaisFacade();
    }

    @Override
    public void addCity(String city) {
        facade.addCity(city);
    }

    @Override
    public void addHall(String city, String hall, int sits) {
        facade.addHall(city,hall,sits);
    }

    @Override
    public void addAdmin(String city, String user, String pass) {
        facade.addAdmin(city, user, pass);
    }

    @Override
    public int addNewShow(String user, String pass, ShowInfo showInfo) {
        return facade.addNewShow(user, pass, showInfo);
    }

    @Override
    public void reserveMemberChairs(int showID, int from, int to) {
        facade.reserveMemberChairs(showID, from, to);
    }

    @Override
    public int newOrder(OrderInfo order) {
        return facade.newOrder(order);
    }
}