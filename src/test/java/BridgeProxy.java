package test.java;


import test.java.auxiliary.OrderInfo;
import test.java.auxiliary.ShowInfo;

public class BridgeProxy implements BridgeProject {
	private BridgeProject real;

	public BridgeProxy() {
		this.real=null;
	}

	public void setRealBridge(BridgeProject real) {
		this.real = real;
	}

	@Override
	public void addCity(String city) {
		if(this.real!=null)
			this.real.addCity(city);
	}

	@Override
	public void addHall(String city, String hall, int sits) {
		if(this.real!=null)
			this.real.addHall(city, hall, sits);
	}

	@Override
	public void addAdmin(String city, String user, String pass) {
		if(this.real!=null)
			this.real.addAdmin(city, user, pass);
	}

	@Override
	public int addNewShow(String user, String pass, ShowInfo showInfo) {
		if(this.real!=null)
			return this.real.addNewShow(user, pass, showInfo);
		return 1;
	}

	@Override
	public void reserveMemberChairs(int showID, int from, int to) {
		if(this.real!=null)
			this.real.reserveMemberChairs(showID, from, to);
	}

	@Override
	public int newOrder(OrderInfo order) {
		if(this.real!=null)
			return this.real.newOrder(order);
		return 1;
	}
}