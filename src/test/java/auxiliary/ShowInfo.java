package test.java.auxiliary;

public class ShowInfo {
	public String city;
	public String hall;
	public String name;
	public String description;
	public long lastOrderTimeDate;
	public long showTimeDate;
	public double ticketCost;
}