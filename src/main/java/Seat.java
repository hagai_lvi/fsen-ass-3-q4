package main.java;

/**
 * Created by hagai_lvi on 12/26/14.
 */
public class Seat {

	/**
	 * Anyone can order this seat
	 */
	public final int FREE_SEAT = 0;

	/**
	 * Only pais members can order this seat
	 */
	public final int RESERVED_FOR_PAIS_ONLY = 1;

	/**
	 * Seat is already taken
	 */
	public final int TAKEN = 2;

	private int orderID = -1;
	private int state;

	public Seat(boolean isReservedForPaisOnly){
		state = isReservedForPaisOnly ? RESERVED_FOR_PAIS_ONLY : FREE_SEAT;
	}

	public Seat() {
		state = FREE_SEAT;
	}

	public boolean freeForAll(){
		return state == FREE_SEAT;
	}

	public boolean freeForPaisMembers(){
		return state == FREE_SEAT || state == RESERVED_FOR_PAIS_ONLY;
	}

	public boolean isSeatTaken(){
		return state == TAKEN;
	}

	public void reserveForPaisMembersOnly() {
		if (isSeatTaken())
			throw new RuntimeException("can't reserve seat because it is already taken");

		state = RESERVED_FOR_PAIS_ONLY;
	}

	public void orderSeat(int orderID) {
		state = TAKEN;
		this.orderID = orderID;
	}
}
