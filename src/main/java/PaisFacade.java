package main.java;

import test.java.auxiliary.OrderInfo;
import test.java.auxiliary.ShowInfo;

import java.util.HashMap;

/**
 * Created by hagai_lvi on 12/23/14.
 */
public class PaisFacade {

	// will store PaisCenters by city name
	HashMap<String,PaisCenter> centers = new HashMap<String, PaisCenter>();


	public void addCity(String city) {
		if (centers.get(city) == null)
			centers.put(city, new PaisCenter(city));
	}

	public void addHall(String city, String hall, int sits) {
		PaisCenter center = centers.get(city);
		if (center == null)
			throw new RuntimeException("cant add Hall because there is no such city " + city);
		center.addHall(hall, sits);
	}

	public void addAdmin(String city, String user, String pass) {
		centers.get(city).addAdmin(user,pass);
	}

	public int addNewShow(String user, String pass, ShowInfo showInfo) {
		ShowInfo clone = cloneShowInfo(showInfo);
        if (! verifyShow(clone))
            return 0;
		String showCity = clone.city;
		PaisCenter center = centers.get(showCity);
		if ( center == null || !center.login(user, pass))
			return 0;
		return center.addShow(clone);
	}

	private ShowInfo cloneShowInfo(ShowInfo showInfo) {
		ShowInfo res = new ShowInfo();
		res.city = showInfo.city;
		res.hall = showInfo.hall;
		res.name = showInfo.name;
		res.description = showInfo.description;
		res.lastOrderTimeDate = showInfo.lastOrderTimeDate;
		res.showTimeDate = showInfo.showTimeDate;
		res.ticketCost = showInfo.ticketCost;
		return res;
	}

	/**
	 * Marks the given seats as reserved for Pais members only
	 */
	public void reserveMemberChairs(int showID, int from, int to) {
		for (PaisCenter c: centers.values()){
			if (c.hasShow(showID))
				c.reserveMemberChairs(showID, from, to);
		}

	}

	public int newOrder(OrderInfo order) {
		for (PaisCenter c: centers.values()){
			if (c.hasShow(order.showId))
				return c.newOrder(order);
		}
		return 0;
	}

    /**
     * Check that customers can still order tickets for this show
     * */
    public static boolean verifyShow(ShowInfo show){
        return
        show.city != null &&
        show.hall != null &&
        show.name != null &&
        show.description != null &&
        show.lastOrderTimeDate <= show.showTimeDate &&
        show.ticketCost > 0;
    }

}
