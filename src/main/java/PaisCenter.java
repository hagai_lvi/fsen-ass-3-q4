package main.java;

import test.java.auxiliary.OrderInfo;
import test.java.auxiliary.ShowInfo;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by hagai_lvi on 12/23/14.
 */
public class PaisCenter {

	private String cityName;
	private HashMap<String,String> admins = new HashMap<String, String>();
	private HashMap<Integer,ShowInfo> shows = new HashMap<Integer,ShowInfo>();
	private HashMap<String,Hall> halls = new HashMap<String, Hall>();
	private HashMap<Integer,Seat[]> orderedSeats = new HashMap<Integer,Seat[]>();
	private static AtomicInteger counter = new AtomicInteger(20);

	public PaisCenter(String cityName){
		this.cityName = cityName;
	}

	public boolean addAdmin(String username, String password){
		if (!admins.containsKey(username)) {
			admins.put(username, password);
			return true;
		}
		return false;

	}

	public boolean login(String username, String password){
		if (username == null || password == null)
			return false;
		return password.equals(admins.get(username));
	}

	public int addShow(ShowInfo show){
		Hall hall = halls.get(show.hall);
		if (hall == null)
			return 0;

		Integer id = UIDGenerator.getUid();
		shows.put(id,show);

		Seat[] seatsReservations = new Seat[hall.getNumOfSits()];
		for (int i=0 ; i<seatsReservations.length ; i++)
			seatsReservations[i] = new Seat();

		orderedSeats.put(id, seatsReservations);

		return id;
	}


	public void addHall(String hall, int sits) {
		if (halls.containsKey(hall))
			throw new RuntimeException(cityName + " already has a hall called " + hall);
		halls.put(hall, new Hall(hall,sits));
	}

	public boolean hasShow(int showID){
		return shows.containsKey(showID);
	}

	public boolean reserveMemberChairs(int showID, int from, int to) {
		Seat[] seats = orderedSeats.get(showID);
		if (seats == null)
			throw new RuntimeException("No such showID " + showID);
		if (from < 0 || to > seats.length)
			throw new RuntimeException("Seats " + from + "-" + to + " doesn't exist in the hall");

		for (int i = from ; i < to ; i++){
			if (! seats[i].freeForPaisMembers()) // can't reserve because seats are already taken
				return false;
		}

		for (int i = from ; i < to ; i++){
			seats[i].reserveForPaisMembersOnly();
		}

		return true;
	}

	private boolean checkIfSeatIsFree(int showID, boolean forPaisMember, int seatNumber){
		return checkIfSeatsAreFree(showID, forPaisMember, seatNumber, seatNumber);
	}

	private boolean checkIfSeatsAreFree(int showID, boolean forPaisMember, int from, int to){
		Seat[] showSeats = orderedSeats.get(showID);
		if (showSeats == null)
			return false;

		if (from < 0 || to > showSeats.length)
			return false;

		for (int i = from ; i <= to ; i++){
			if (! (showSeats[i].freeForAll() ||
					(showSeats[i].freeForPaisMembers() && forPaisMember) ))
				return false;
		}

		return true;
	}

	/**
	 * order seats in a show
	 * @return 0 if couldn't order, or orderID if order was placed
	 */
	public synchronized int newOrder(OrderInfo order){
		if (! verifyOrder(order))
			return 0;

        ShowInfo show = shows.get(order.showId);
        if (show == null || show.lastOrderTimeDate < System.currentTimeMillis())
            return 0;

		boolean isPaisMember = order.memberId != null;
		boolean canOrder = true;

		//verify seats are available
		for (int i : order.chairsIds)
				canOrder = canOrder && checkIfSeatIsFree(order.showId, isPaisMember, i);
		if (!canOrder)
			return 0;


		Seat[] showSeats = orderedSeats.get(order.showId);
		int orderID = UIDGenerator.getUid();
		for (int i : order.chairsIds)
			showSeats[i].orderSeat(orderID);

		return orderID;
	}

	/**
	 * Verify that no field is null
	 */
	public static boolean verifyOrder(OrderInfo order){
		return (
		order.showId >= 1 &&
		order.name != null &&
		order.phone != null &&
		order.chairsIds != null &&
        order.chairsIds.length > 0);

	}
}
