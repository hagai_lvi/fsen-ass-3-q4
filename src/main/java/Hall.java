package main.java;

/**
 * Represents a hall that can have shows
 */
public class Hall {
	private String name;
	private int numOfSits;
	public Hall(String hall, int sits) {
		name = hall;
		numOfSits = sits;
	}

	public int getNumOfSits() {
		return numOfSits;
	}

	public String getName() {
		return name;
	}
}
