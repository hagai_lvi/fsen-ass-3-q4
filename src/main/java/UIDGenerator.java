package main.java;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by hagai_lvi on 12/26/14.
 */
public class UIDGenerator {
	private static AtomicInteger counter = new AtomicInteger(20);

	public static synchronized int getUid(){
		return counter.addAndGet(1);
	}
}
